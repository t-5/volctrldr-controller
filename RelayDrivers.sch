EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 5 8
Title "VolCtrlDR-Controller"
Date "2020-01-27"
Rev "0.4"
Comp "T5! DIY Audio"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L SamacSys_Parts:TPL7407LAPWR Q1
U 1 1 5DF76DA0
P 1700 950
F 0 "Q1" H 2250 1215 50  0000 C CNN
F 1 "TPL7407LAPWR" H 2250 1124 50  0000 C CNN
F 2 "SamacSys_Parts:SOP65P640X120-16N" H 2650 1050 50  0001 L CNN
F 3 "http://www.ti.com/lit/gpn/tpl7407la" H 2650 950 50  0001 L CNN
F 4 "30-V, 7-Channel NMOS Array Low-Side Driver" H 2650 850 50  0001 L CNN "Description"
F 5 "1.2" H 2650 750 50  0001 L CNN "Height"
F 6 "595-TPL7407LAPWR" H 2650 650 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.com/Search/Refine.aspx?Keyword=595-TPL7407LAPWR" H 2650 550 50  0001 L CNN "Mouser Price/Stock"
F 8 "Texas Instruments" H 2650 450 50  0001 L CNN "Manufacturer_Name"
F 9 "TPL7407LAPWR" H 2650 350 50  0001 L CNN "Manufacturer_Part_Number"
	1    1700 950 
	1    0    0    -1  
$EndComp
Wire Wire Line
	2850 1700 2800 1650
$Comp
L power:+12V #PWR0145
U 1 1 5DF7784E
P 2850 1700
F 0 "#PWR0145" H 2850 1550 50  0001 C CNN
F 1 "+12V" V 2865 1828 50  0000 L CNN
F 2 "" H 2850 1700 50  0001 C CNN
F 3 "" H 2850 1700 50  0001 C CNN
	1    2850 1700
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0146
U 1 1 5DF79062
P 1650 1700
F 0 "#PWR0146" H 1650 1450 50  0001 C CNN
F 1 "GND" V 1655 1572 50  0000 R CNN
F 2 "" H 1650 1700 50  0001 C CNN
F 3 "" H 1650 1700 50  0001 C CNN
	1    1650 1700
	0    1    1    0   
$EndComp
Wire Wire Line
	1650 1700 1700 1650
$Comp
L SamacSys_Parts:TPL7407LAPWR Q2
U 1 1 5DF7BF9E
P 1700 2300
F 0 "Q2" H 2250 2565 50  0000 C CNN
F 1 "TPL7407LAPWR" H 2250 2474 50  0000 C CNN
F 2 "SamacSys_Parts:SOP65P640X120-16N" H 2650 2400 50  0001 L CNN
F 3 "http://www.ti.com/lit/gpn/tpl7407la" H 2650 2300 50  0001 L CNN
F 4 "30-V, 7-Channel NMOS Array Low-Side Driver" H 2650 2200 50  0001 L CNN "Description"
F 5 "1.2" H 2650 2100 50  0001 L CNN "Height"
F 6 "595-TPL7407LAPWR" H 2650 2000 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.com/Search/Refine.aspx?Keyword=595-TPL7407LAPWR" H 2650 1900 50  0001 L CNN "Mouser Price/Stock"
F 8 "Texas Instruments" H 2650 1800 50  0001 L CNN "Manufacturer_Name"
F 9 "TPL7407LAPWR" H 2650 1700 50  0001 L CNN "Manufacturer_Part_Number"
	1    1700 2300
	1    0    0    -1  
$EndComp
Wire Wire Line
	2850 3050 2800 3000
$Comp
L power:+12V #PWR0147
U 1 1 5DF7BFA9
P 2850 3050
F 0 "#PWR0147" H 2850 2900 50  0001 C CNN
F 1 "+12V" V 2865 3178 50  0000 L CNN
F 2 "" H 2850 3050 50  0001 C CNN
F 3 "" H 2850 3050 50  0001 C CNN
	1    2850 3050
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0148
U 1 1 5DF7BFB3
P 1650 3050
F 0 "#PWR0148" H 1650 2800 50  0001 C CNN
F 1 "GND" V 1655 2922 50  0000 R CNN
F 2 "" H 1650 3050 50  0001 C CNN
F 3 "" H 1650 3050 50  0001 C CNN
	1    1650 3050
	0    1    1    0   
$EndComp
Wire Wire Line
	1650 3050 1700 3000
$Comp
L SamacSys_Parts:TPL7407LAPWR Q3
U 1 1 5DF7E6E4
P 1700 3650
F 0 "Q3" H 2250 3915 50  0000 C CNN
F 1 "TPL7407LAPWR" H 2250 3824 50  0000 C CNN
F 2 "SamacSys_Parts:SOP65P640X120-16N" H 2650 3750 50  0001 L CNN
F 3 "http://www.ti.com/lit/gpn/tpl7407la" H 2650 3650 50  0001 L CNN
F 4 "30-V, 7-Channel NMOS Array Low-Side Driver" H 2650 3550 50  0001 L CNN "Description"
F 5 "1.2" H 2650 3450 50  0001 L CNN "Height"
F 6 "595-TPL7407LAPWR" H 2650 3350 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.com/Search/Refine.aspx?Keyword=595-TPL7407LAPWR" H 2650 3250 50  0001 L CNN "Mouser Price/Stock"
F 8 "Texas Instruments" H 2650 3150 50  0001 L CNN "Manufacturer_Name"
F 9 "TPL7407LAPWR" H 2650 3050 50  0001 L CNN "Manufacturer_Part_Number"
	1    1700 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	2850 4400 2800 4350
$Comp
L power:+12V #PWR0149
U 1 1 5DF7E6EF
P 2850 4400
F 0 "#PWR0149" H 2850 4250 50  0001 C CNN
F 1 "+12V" V 2865 4528 50  0000 L CNN
F 2 "" H 2850 4400 50  0001 C CNN
F 3 "" H 2850 4400 50  0001 C CNN
	1    2850 4400
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0150
U 1 1 5DF7E6F9
P 1650 4400
F 0 "#PWR0150" H 1650 4150 50  0001 C CNN
F 1 "GND" V 1655 4272 50  0000 R CNN
F 2 "" H 1650 4400 50  0001 C CNN
F 3 "" H 1650 4400 50  0001 C CNN
	1    1650 4400
	0    1    1    0   
$EndComp
Wire Wire Line
	1650 4400 1700 4350
$Comp
L SamacSys_Parts:TPL7407LAPWR Q4
U 1 1 5DF87383
P 4650 950
F 0 "Q4" H 5200 1215 50  0000 C CNN
F 1 "TPL7407LAPWR" H 5200 1124 50  0000 C CNN
F 2 "SamacSys_Parts:SOP65P640X120-16N" H 5600 1050 50  0001 L CNN
F 3 "http://www.ti.com/lit/gpn/tpl7407la" H 5600 950 50  0001 L CNN
F 4 "30-V, 7-Channel NMOS Array Low-Side Driver" H 5600 850 50  0001 L CNN "Description"
F 5 "1.2" H 5600 750 50  0001 L CNN "Height"
F 6 "595-TPL7407LAPWR" H 5600 650 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.com/Search/Refine.aspx?Keyword=595-TPL7407LAPWR" H 5600 550 50  0001 L CNN "Mouser Price/Stock"
F 8 "Texas Instruments" H 5600 450 50  0001 L CNN "Manufacturer_Name"
F 9 "TPL7407LAPWR" H 5600 350 50  0001 L CNN "Manufacturer_Part_Number"
	1    4650 950 
	1    0    0    -1  
$EndComp
Wire Wire Line
	5800 1700 5750 1650
$Comp
L power:+12V #PWR0151
U 1 1 5DF8738E
P 5800 1700
F 0 "#PWR0151" H 5800 1550 50  0001 C CNN
F 1 "+12V" V 5815 1828 50  0000 L CNN
F 2 "" H 5800 1700 50  0001 C CNN
F 3 "" H 5800 1700 50  0001 C CNN
	1    5800 1700
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0152
U 1 1 5DF87398
P 4600 1700
F 0 "#PWR0152" H 4600 1450 50  0001 C CNN
F 1 "GND" V 4605 1572 50  0000 R CNN
F 2 "" H 4600 1700 50  0001 C CNN
F 3 "" H 4600 1700 50  0001 C CNN
	1    4600 1700
	0    1    1    0   
$EndComp
Wire Wire Line
	4600 1700 4650 1650
$Comp
L SamacSys_Parts:TPL7407LAPWR Q5
U 1 1 5DF873A9
P 4650 2300
F 0 "Q5" H 5200 2565 50  0000 C CNN
F 1 "TPL7407LAPWR" H 5200 2474 50  0000 C CNN
F 2 "SamacSys_Parts:SOP65P640X120-16N" H 5600 2400 50  0001 L CNN
F 3 "http://www.ti.com/lit/gpn/tpl7407la" H 5600 2300 50  0001 L CNN
F 4 "30-V, 7-Channel NMOS Array Low-Side Driver" H 5600 2200 50  0001 L CNN "Description"
F 5 "1.2" H 5600 2100 50  0001 L CNN "Height"
F 6 "595-TPL7407LAPWR" H 5600 2000 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.com/Search/Refine.aspx?Keyword=595-TPL7407LAPWR" H 5600 1900 50  0001 L CNN "Mouser Price/Stock"
F 8 "Texas Instruments" H 5600 1800 50  0001 L CNN "Manufacturer_Name"
F 9 "TPL7407LAPWR" H 5600 1700 50  0001 L CNN "Manufacturer_Part_Number"
	1    4650 2300
	1    0    0    -1  
$EndComp
Wire Wire Line
	5800 3050 5750 3000
$Comp
L power:+12V #PWR0153
U 1 1 5DF873B4
P 5800 3050
F 0 "#PWR0153" H 5800 2900 50  0001 C CNN
F 1 "+12V" V 5815 3178 50  0000 L CNN
F 2 "" H 5800 3050 50  0001 C CNN
F 3 "" H 5800 3050 50  0001 C CNN
	1    5800 3050
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0154
U 1 1 5DF873BE
P 4600 3050
F 0 "#PWR0154" H 4600 2800 50  0001 C CNN
F 1 "GND" V 4605 2922 50  0000 R CNN
F 2 "" H 4600 3050 50  0001 C CNN
F 3 "" H 4600 3050 50  0001 C CNN
	1    4600 3050
	0    1    1    0   
$EndComp
Wire Wire Line
	4600 3050 4650 3000
$Comp
L SamacSys_Parts:TPL7407LAPWR Q6
U 1 1 5DF873CF
P 4650 3650
F 0 "Q6" H 5200 3915 50  0000 C CNN
F 1 "TPL7407LAPWR" H 5200 3824 50  0000 C CNN
F 2 "SamacSys_Parts:SOP65P640X120-16N" H 5600 3750 50  0001 L CNN
F 3 "http://www.ti.com/lit/gpn/tpl7407la" H 5600 3650 50  0001 L CNN
F 4 "30-V, 7-Channel NMOS Array Low-Side Driver" H 5600 3550 50  0001 L CNN "Description"
F 5 "1.2" H 5600 3450 50  0001 L CNN "Height"
F 6 "595-TPL7407LAPWR" H 5600 3350 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.com/Search/Refine.aspx?Keyword=595-TPL7407LAPWR" H 5600 3250 50  0001 L CNN "Mouser Price/Stock"
F 8 "Texas Instruments" H 5600 3150 50  0001 L CNN "Manufacturer_Name"
F 9 "TPL7407LAPWR" H 5600 3050 50  0001 L CNN "Manufacturer_Part_Number"
	1    4650 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	5800 4400 5750 4350
$Comp
L power:+12V #PWR0155
U 1 1 5DF873DA
P 5800 4400
F 0 "#PWR0155" H 5800 4250 50  0001 C CNN
F 1 "+12V" V 5815 4528 50  0000 L CNN
F 2 "" H 5800 4400 50  0001 C CNN
F 3 "" H 5800 4400 50  0001 C CNN
	1    5800 4400
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0156
U 1 1 5DF873E4
P 4550 4450
F 0 "#PWR0156" H 4550 4200 50  0001 C CNN
F 1 "GND" V 4555 4322 50  0000 R CNN
F 2 "" H 4550 4450 50  0001 C CNN
F 3 "" H 4550 4450 50  0001 C CNN
	1    4550 4450
	1    0    0    -1  
$EndComp
$Comp
L Device:C C17
U 1 1 5DF8AA37
P 7650 1150
F 0 "C17" H 7765 1196 50  0000 L CNN
F 1 "100n" H 7765 1105 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 7688 1000 50  0001 C CNN
F 3 "~" H 7650 1150 50  0001 C CNN
	1    7650 1150
	1    0    0    -1  
$EndComp
$Comp
L power:+12V #PWR0157
U 1 1 5DF8BAB1
P 7650 900
F 0 "#PWR0157" H 7650 750 50  0001 C CNN
F 1 "+12V" H 7665 1073 50  0000 C CNN
F 2 "" H 7650 900 50  0001 C CNN
F 3 "" H 7650 900 50  0001 C CNN
	1    7650 900 
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0158
U 1 1 5DF8C3C4
P 7650 1400
F 0 "#PWR0158" H 7650 1150 50  0001 C CNN
F 1 "GND" H 7655 1227 50  0000 C CNN
F 2 "" H 7650 1400 50  0001 C CNN
F 3 "" H 7650 1400 50  0001 C CNN
	1    7650 1400
	1    0    0    -1  
$EndComp
Wire Wire Line
	7650 1400 7650 1350
Wire Wire Line
	7650 900  7650 950 
$Comp
L Device:C C18
U 1 1 5DF8DBEF
P 8050 1150
F 0 "C18" H 8165 1196 50  0000 L CNN
F 1 "100n" H 8165 1105 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 8088 1000 50  0001 C CNN
F 3 "~" H 8050 1150 50  0001 C CNN
	1    8050 1150
	1    0    0    -1  
$EndComp
Wire Wire Line
	8050 1000 8050 950 
Wire Wire Line
	8050 950  7650 950 
Connection ~ 7650 950 
Wire Wire Line
	7650 950  7650 1000
Wire Wire Line
	8050 1300 8050 1350
Wire Wire Line
	8050 1350 7650 1350
Connection ~ 7650 1350
Wire Wire Line
	7650 1350 7650 1300
$Comp
L Device:C C19
U 1 1 5DF902A0
P 8450 1150
F 0 "C19" H 8565 1196 50  0000 L CNN
F 1 "100n" H 8565 1105 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 8488 1000 50  0001 C CNN
F 3 "~" H 8450 1150 50  0001 C CNN
	1    8450 1150
	1    0    0    -1  
$EndComp
Wire Wire Line
	8450 1000 8450 950 
Wire Wire Line
	8450 950  8050 950 
Wire Wire Line
	8450 1300 8450 1350
Wire Wire Line
	8450 1350 8050 1350
Connection ~ 8050 950 
Connection ~ 8050 1350
$Comp
L Device:C C20
U 1 1 5DF9C1B9
P 8850 1150
F 0 "C20" H 8965 1196 50  0000 L CNN
F 1 "100n" H 8965 1105 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 8888 1000 50  0001 C CNN
F 3 "~" H 8850 1150 50  0001 C CNN
	1    8850 1150
	1    0    0    -1  
$EndComp
Wire Wire Line
	8850 1000 8850 950 
Wire Wire Line
	8850 950  8450 950 
Wire Wire Line
	8850 1300 8850 1350
Wire Wire Line
	8850 1350 8450 1350
$Comp
L Device:C C21
U 1 1 5DF9C1C7
P 9250 1150
F 0 "C21" H 9365 1196 50  0000 L CNN
F 1 "100n" H 9365 1105 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 9288 1000 50  0001 C CNN
F 3 "~" H 9250 1150 50  0001 C CNN
	1    9250 1150
	1    0    0    -1  
$EndComp
Wire Wire Line
	9250 1000 9250 950 
Wire Wire Line
	9250 950  8850 950 
Wire Wire Line
	9250 1300 9250 1350
Wire Wire Line
	9250 1350 8850 1350
Connection ~ 8850 950 
Connection ~ 8850 1350
Connection ~ 8450 950 
Connection ~ 8450 1350
$Comp
L Device:C C22
U 1 1 5DFA293E
P 9650 1150
F 0 "C22" H 9765 1196 50  0000 L CNN
F 1 "100n" H 9765 1105 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 9688 1000 50  0001 C CNN
F 3 "~" H 9650 1150 50  0001 C CNN
	1    9650 1150
	1    0    0    -1  
$EndComp
Wire Wire Line
	9650 1000 9650 950 
Wire Wire Line
	9650 950  9250 950 
Wire Wire Line
	9650 1300 9650 1350
Wire Wire Line
	9650 1350 9250 1350
Connection ~ 9250 950 
Connection ~ 9250 1350
$Comp
L SamacSys_Parts:TPL7407LAPWR Q7
U 1 1 5E18DD19
P 7750 3650
F 0 "Q7" H 8300 3915 50  0000 C CNN
F 1 "TPL7407LAPWR" H 8300 3824 50  0000 C CNN
F 2 "SamacSys_Parts:SOP65P640X120-16N" H 8700 3750 50  0001 L CNN
F 3 "http://www.ti.com/lit/gpn/tpl7407la" H 8700 3650 50  0001 L CNN
F 4 "30-V, 7-Channel NMOS Array Low-Side Driver" H 8700 3550 50  0001 L CNN "Description"
F 5 "1.2" H 8700 3450 50  0001 L CNN "Height"
F 6 "595-TPL7407LAPWR" H 8700 3350 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.com/Search/Refine.aspx?Keyword=595-TPL7407LAPWR" H 8700 3250 50  0001 L CNN "Mouser Price/Stock"
F 8 "Texas Instruments" H 8700 3150 50  0001 L CNN "Manufacturer_Name"
F 9 "TPL7407LAPWR" H 8700 3050 50  0001 L CNN "Manufacturer_Part_Number"
	1    7750 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	8900 4400 8850 4350
$Comp
L power:+12V #PWR0159
U 1 1 5E18DD24
P 8900 4400
F 0 "#PWR0159" H 8900 4250 50  0001 C CNN
F 1 "+12V" V 8915 4528 50  0000 L CNN
F 2 "" H 8900 4400 50  0001 C CNN
F 3 "" H 8900 4400 50  0001 C CNN
	1    8900 4400
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0160
U 1 1 5E18DD2E
P 7600 4400
F 0 "#PWR0160" H 7600 4150 50  0001 C CNN
F 1 "GND" V 7605 4272 50  0000 R CNN
F 2 "" H 7600 4400 50  0001 C CNN
F 3 "" H 7600 4400 50  0001 C CNN
	1    7600 4400
	0    1    1    0   
$EndComp
$Comp
L Device:C C23
U 1 1 5E194889
P 10050 1150
F 0 "C23" H 10165 1196 50  0000 L CNN
F 1 "100n" H 10165 1105 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 10088 1000 50  0001 C CNN
F 3 "~" H 10050 1150 50  0001 C CNN
	1    10050 1150
	1    0    0    -1  
$EndComp
Wire Wire Line
	10050 1000 10050 950 
Wire Wire Line
	10050 950  9650 950 
Wire Wire Line
	10050 1300 10050 1350
Wire Wire Line
	10050 1350 9650 1350
Connection ~ 9650 950 
Connection ~ 9650 1350
Wire Wire Line
	2800 950  2900 950 
Wire Wire Line
	2800 1050 2900 1050
Wire Wire Line
	2800 1150 2900 1150
Wire Wire Line
	2800 1250 2900 1250
Wire Wire Line
	2800 1350 2900 1350
Wire Wire Line
	2800 1450 2900 1450
Wire Wire Line
	2800 1550 2900 1550
Wire Wire Line
	2800 2300 2900 2300
Wire Wire Line
	2800 2400 2900 2400
Wire Wire Line
	2800 2500 2900 2500
Wire Wire Line
	2800 2600 2900 2600
Wire Wire Line
	2800 2700 2900 2700
Wire Wire Line
	2800 2800 2900 2800
Wire Wire Line
	2800 2900 2900 2900
Wire Wire Line
	2800 3650 2900 3650
Wire Wire Line
	2800 3750 2900 3750
Wire Wire Line
	2800 3850 2900 3850
Wire Wire Line
	2800 3950 2900 3950
Wire Wire Line
	2800 4050 2900 4050
Wire Wire Line
	2800 4150 2900 4150
Wire Wire Line
	2800 4250 2900 4250
Wire Wire Line
	5750 950  5850 950 
Wire Wire Line
	5750 1050 5850 1050
Wire Wire Line
	5750 1150 5850 1150
Wire Wire Line
	5750 1250 5850 1250
Wire Wire Line
	5750 1350 5850 1350
Wire Wire Line
	5750 1450 5850 1450
Wire Wire Line
	5750 1550 5850 1550
Wire Wire Line
	5750 2300 5850 2300
Wire Wire Line
	5750 2400 5850 2400
Wire Wire Line
	5750 2500 5850 2500
Wire Wire Line
	5750 2600 5850 2600
Wire Wire Line
	5750 2700 5850 2700
Wire Wire Line
	5750 2800 5850 2800
Wire Wire Line
	5750 2900 5850 2900
Text GLabel 2900 950  2    50   Input ~ 0
RY_32DB_1
Text GLabel 2900 1050 2    50   Input ~ 0
RY_16DB_1
Text GLabel 2900 1150 2    50   Input ~ 0
RY_8DB_1
Text GLabel 2900 1250 2    50   Input ~ 0
RY_4DB_1
Text GLabel 2900 1350 2    50   Input ~ 0
RY_2DB_1
Text GLabel 2900 1450 2    50   Input ~ 0
RY_1DB_1
Text GLabel 2900 1550 2    50   Input ~ 0
RY_MUTE_LR_1
Text GLabel 2900 2300 2    50   Input ~ 0
RY_32DB_2
Text GLabel 2900 2400 2    50   Input ~ 0
RY_16DB_2
Text GLabel 2900 2500 2    50   Input ~ 0
RY_8DB_2
Text GLabel 2900 2600 2    50   Input ~ 0
RY_4DB_2
Text GLabel 2900 2700 2    50   Input ~ 0
RY_2DB_2
Text GLabel 2900 2800 2    50   Input ~ 0
RY_1DB_2
Text GLabel 2900 2900 2    50   Input ~ 0
RY_MUTE_LR_2
Text GLabel 2900 3650 2    50   Input ~ 0
RY_32DB_3
Text GLabel 2900 3750 2    50   Input ~ 0
RY_16DB_3
Text GLabel 2900 3850 2    50   Input ~ 0
RY_8DB_3
Text GLabel 2900 3950 2    50   Input ~ 0
RY_4DB_3
Text GLabel 2900 4050 2    50   Input ~ 0
RY_2DB_3
Text GLabel 2900 4150 2    50   Input ~ 0
RY_1DB_3
Text GLabel 2900 4250 2    50   Input ~ 0
RY_MUTE_LR_3
Text GLabel 5850 2300 2    50   Input ~ 0
RY_32DB_5
Text GLabel 5850 2400 2    50   Input ~ 0
RY_16DB_5
Text GLabel 5850 2500 2    50   Input ~ 0
RY_8DB_5
Text GLabel 5850 2600 2    50   Input ~ 0
RY_4DB_5
Text GLabel 5850 2700 2    50   Input ~ 0
RY_2DB_5
Text GLabel 5850 2800 2    50   Input ~ 0
RY_1DB_5
Text GLabel 5850 2900 2    50   Input ~ 0
RY_MUTE_LR_5
Text GLabel 5850 950  2    50   Input ~ 0
RY_32DB_4
Text GLabel 5850 1050 2    50   Input ~ 0
RY_16DB_4
Text GLabel 5850 1150 2    50   Input ~ 0
RY_8DB_4
Text GLabel 5850 1250 2    50   Input ~ 0
RY_4DB_4
Text GLabel 5850 1350 2    50   Input ~ 0
RY_2DB_4
Text GLabel 5850 1450 2    50   Input ~ 0
RY_1DB_4
Text GLabel 5850 1550 2    50   Input ~ 0
RY_MUTE_LR_4
Text GLabel 5850 3650 2    50   Input ~ 0
RY_MUTE_PA1L
Wire Wire Line
	5850 3650 5750 3650
Wire Wire Line
	5750 3750 5850 3750
Wire Wire Line
	5850 3850 5750 3850
Wire Wire Line
	5850 3950 5750 3950
Wire Wire Line
	5850 4050 5750 4050
Wire Wire Line
	5850 4150 5750 4150
Text GLabel 5850 3750 2    50   Input ~ 0
RY_MUTE_PA1R
Text GLabel 5850 3850 2    50   Input ~ 0
RY_MUTE_PA2L
Text GLabel 5850 3950 2    50   Input ~ 0
RY_MUTE_PA2R
Text GLabel 5850 4150 2    50   Input ~ 0
RY_MUTE_PA3R
Text GLabel 5850 4050 2    50   Input ~ 0
RY_MUTE_PA3L
Text GLabel 8950 3650 2    50   Input ~ 0
RY_MUTE_PA4L
Text GLabel 8950 3750 2    50   Input ~ 0
RY_MUTE_PA4R
Text GLabel 8950 3850 2    50   Input ~ 0
RY_MUTE_PA5L
Text GLabel 8950 3950 2    50   Input ~ 0
RY_MUTE_PA5R
Wire Wire Line
	8950 3950 8850 3950
Wire Wire Line
	8850 3850 8950 3850
Wire Wire Line
	8950 3750 8850 3750
Wire Wire Line
	8850 3650 8950 3650
NoConn ~ 5750 4250
NoConn ~ 8850 4050
NoConn ~ 8850 4150
NoConn ~ 8850 4250
Text GLabel 1600 950  0    50   Input ~ 0
LL_32DB_1
Wire Wire Line
	1600 950  1700 950 
Text GLabel 1600 1050 0    50   Input ~ 0
LL_16DB_1
Wire Wire Line
	1600 1050 1700 1050
Text GLabel 1600 1150 0    50   Input ~ 0
LL_8DB_1
Wire Wire Line
	1600 1150 1700 1150
Text GLabel 1600 1250 0    50   Input ~ 0
LL_4DB_1
Wire Wire Line
	1600 1250 1700 1250
Text GLabel 1600 1350 0    50   Input ~ 0
LL_2DB_1
Wire Wire Line
	1600 1350 1700 1350
Text GLabel 1600 1450 0    50   Input ~ 0
LL_1DB_1
Wire Wire Line
	1600 1450 1700 1450
Text GLabel 1600 1550 0    50   Input ~ 0
LL2_MUTE_LR1
Wire Wire Line
	1600 1550 1700 1550
Text GLabel 1600 2300 0    50   Input ~ 0
LL_32DB_2
Wire Wire Line
	1600 2300 1700 2300
Text GLabel 1600 2400 0    50   Input ~ 0
LL_16DB_2
Wire Wire Line
	1600 2400 1700 2400
Text GLabel 1600 2500 0    50   Input ~ 0
LL_8DB_2
Wire Wire Line
	1600 2500 1700 2500
Text GLabel 1600 2600 0    50   Input ~ 0
LL_4DB_2
Wire Wire Line
	1600 2600 1700 2600
Text GLabel 1600 2700 0    50   Input ~ 0
LL_2DB_2
Wire Wire Line
	1600 2700 1700 2700
Text GLabel 1600 2800 0    50   Input ~ 0
LL_1DB_2
Wire Wire Line
	1600 2800 1700 2800
Text GLabel 1600 2900 0    50   Input ~ 0
LL2_MUTE_LR2
Wire Wire Line
	1600 2900 1700 2900
Text GLabel 4550 950  0    50   Input ~ 0
LL_32DB_4
Wire Wire Line
	4550 950  4650 950 
Text GLabel 4550 1050 0    50   Input ~ 0
LL_16DB_4
Wire Wire Line
	4550 1050 4650 1050
Text GLabel 4550 1150 0    50   Input ~ 0
LL_8DB_4
Wire Wire Line
	4550 1150 4650 1150
Text GLabel 4550 1250 0    50   Input ~ 0
LL_4DB_4
Wire Wire Line
	4550 1250 4650 1250
Text GLabel 4550 1350 0    50   Input ~ 0
LL_2DB_4
Wire Wire Line
	4550 1350 4650 1350
Text GLabel 4550 1450 0    50   Input ~ 0
LL_1DB_4
Wire Wire Line
	4550 1450 4650 1450
Text GLabel 4550 1550 0    50   Input ~ 0
LL2_MUTE_LR4
Wire Wire Line
	4550 1550 4650 1550
Text GLabel 4550 2300 0    50   Input ~ 0
LL_32DB_5
Wire Wire Line
	4550 2300 4650 2300
Text GLabel 4550 2400 0    50   Input ~ 0
LL_16DB_5
Wire Wire Line
	4550 2400 4650 2400
Text GLabel 4550 2500 0    50   Input ~ 0
LL_8DB_5
Wire Wire Line
	4550 2500 4650 2500
Text GLabel 4550 2600 0    50   Input ~ 0
LL_4DB_5
Wire Wire Line
	4550 2600 4650 2600
Text GLabel 4550 2700 0    50   Input ~ 0
LL_2DB_5
Wire Wire Line
	4550 2700 4650 2700
Text GLabel 4550 2800 0    50   Input ~ 0
LL_1DB_5
Wire Wire Line
	4550 2800 4650 2800
Text GLabel 4550 2900 0    50   Input ~ 0
LL2_MUTE_LR5
Wire Wire Line
	4550 2900 4650 2900
Text GLabel 1600 3650 0    50   Input ~ 0
LL_32DB_3
Wire Wire Line
	1600 3650 1700 3650
Text GLabel 1600 3750 0    50   Input ~ 0
LL_16DB_3
Wire Wire Line
	1600 3750 1700 3750
Text GLabel 1600 3850 0    50   Input ~ 0
LL_8DB_3
Wire Wire Line
	1600 3850 1700 3850
Text GLabel 1600 3950 0    50   Input ~ 0
LL_4DB_3
Wire Wire Line
	1600 3950 1700 3950
Text GLabel 1600 4050 0    50   Input ~ 0
LL_2DB_3
Wire Wire Line
	1600 4050 1700 4050
Text GLabel 1600 4150 0    50   Input ~ 0
LL_1DB_3
Wire Wire Line
	1600 4150 1700 4150
Text GLabel 1600 4250 0    50   Input ~ 0
LL2_MUTE_LR3
Wire Wire Line
	1600 4250 1700 4250
Text GLabel 4550 3650 0    50   Input ~ 0
LL2_MUTE_PA1L
Wire Wire Line
	4550 3650 4650 3650
Text GLabel 4550 3750 0    50   Input ~ 0
LL2_MUTE_PA1R
Wire Wire Line
	4550 3750 4650 3750
Text GLabel 4550 3850 0    50   Input ~ 0
LL2_MUTE_PA2L
Wire Wire Line
	4550 3850 4650 3850
Text GLabel 4550 3950 0    50   Input ~ 0
LL2_MUTE_PA2R
Wire Wire Line
	4550 3950 4650 3950
Text GLabel 4550 4050 0    50   Input ~ 0
LL2_MUTE_PA3L
Wire Wire Line
	4550 4050 4650 4050
Text GLabel 4550 4150 0    50   Input ~ 0
LL2_MUTE_PA3R
Wire Wire Line
	4550 4150 4650 4150
Text GLabel 7650 3650 0    50   Input ~ 0
LL2_MUTE_PA4L
Wire Wire Line
	7650 3650 7750 3650
Text GLabel 7650 3750 0    50   Input ~ 0
LL2_MUTE_PA4R
Wire Wire Line
	7650 3750 7750 3750
Text GLabel 7650 3850 0    50   Input ~ 0
LL2_MUTE_PA5L
Wire Wire Line
	7650 3850 7750 3850
Text GLabel 7650 3950 0    50   Input ~ 0
LL2_MUTE_PA5R
Wire Wire Line
	7650 3950 7750 3950
Wire Wire Line
	7750 4050 7650 4050
Wire Wire Line
	7650 4050 7650 4150
Wire Wire Line
	7650 4150 7750 4150
Wire Wire Line
	7750 4250 7650 4250
Wire Wire Line
	7650 4250 7650 4150
Connection ~ 7650 4150
Wire Wire Line
	7750 4350 7650 4350
Wire Wire Line
	7650 4350 7650 4250
Connection ~ 7650 4250
Wire Wire Line
	7600 4400 7650 4350
Connection ~ 7650 4350
$Comp
L SamacSys_Parts:74HC08DB,118 IC8
U 1 1 5EE07F08
P 1700 6600
F 0 "IC8" H 2200 6865 50  0000 C CNN
F 1 "74HC08DB,118" H 2200 6774 50  0000 C CNN
F 2 "SamacSys_Parts:SOP65P775X200-14N" H 3100 6700 50  0001 L CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/74HC_HCT08.pdf" H 3100 6600 50  0001 L CNN
	1    1700 6600
	1    0    0    -1  
$EndComp
Text GLabel 1550 6600 0    50   Input ~ 0
LL_MUTE_LR5
Text GLabel 1550 6700 0    50   Input ~ 0
PG_CMB
Wire Wire Line
	1550 6600 1700 6600
Wire Wire Line
	1550 6700 1700 6700
Text GLabel 1550 6800 0    50   Input ~ 0
LL2_MUTE_LR5
Wire Wire Line
	1550 6800 1700 6800
$Comp
L power:GND #PWR0171
U 1 1 5EE3AFAB
P 1600 7300
F 0 "#PWR0171" H 1600 7050 50  0001 C CNN
F 1 "GND" V 1605 7172 50  0000 R CNN
F 2 "" H 1600 7300 50  0001 C CNN
F 3 "" H 1600 7300 50  0001 C CNN
	1    1600 7300
	1    0    0    -1  
$EndComp
Wire Wire Line
	1700 7200 1600 7200
Wire Wire Line
	1600 7200 1600 7300
$Comp
L power:+3V3 #PWR0172
U 1 1 5EE43082
P 2800 6500
F 0 "#PWR0172" H 2800 6350 50  0001 C CNN
F 1 "+3V3" H 2815 6673 50  0000 C CNN
F 2 "" H 2800 6500 50  0001 C CNN
F 3 "" H 2800 6500 50  0001 C CNN
	1    2800 6500
	1    0    0    -1  
$EndComp
Wire Wire Line
	2800 6500 2800 6600
Wire Wire Line
	2800 6600 2700 6600
$Comp
L Device:C C52
U 1 1 5EE4ECF0
P 7650 5450
F 0 "C52" H 7765 5496 50  0000 L CNN
F 1 "100n" H 7765 5405 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 7688 5300 50  0001 C CNN
F 3 "~" H 7650 5450 50  0001 C CNN
	1    7650 5450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0173
U 1 1 5EE4ED04
P 7650 5700
F 0 "#PWR0173" H 7650 5450 50  0001 C CNN
F 1 "GND" H 7655 5527 50  0000 C CNN
F 2 "" H 7650 5700 50  0001 C CNN
F 3 "" H 7650 5700 50  0001 C CNN
	1    7650 5700
	1    0    0    -1  
$EndComp
Wire Wire Line
	7650 5700 7650 5650
Wire Wire Line
	7650 5200 7650 5250
$Comp
L Device:C C53
U 1 1 5EE4ED10
P 8050 5450
F 0 "C53" H 8165 5496 50  0000 L CNN
F 1 "100n" H 8165 5405 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 8088 5300 50  0001 C CNN
F 3 "~" H 8050 5450 50  0001 C CNN
	1    8050 5450
	1    0    0    -1  
$EndComp
Wire Wire Line
	8050 5300 8050 5250
Wire Wire Line
	8050 5250 7650 5250
Connection ~ 7650 5250
Wire Wire Line
	7650 5250 7650 5300
Wire Wire Line
	8050 5600 8050 5650
Wire Wire Line
	8050 5650 7650 5650
Connection ~ 7650 5650
Wire Wire Line
	7650 5650 7650 5600
$Comp
L Device:C C54
U 1 1 5EE4ED22
P 8450 5450
F 0 "C54" H 8565 5496 50  0000 L CNN
F 1 "100n" H 8565 5405 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 8488 5300 50  0001 C CNN
F 3 "~" H 8450 5450 50  0001 C CNN
	1    8450 5450
	1    0    0    -1  
$EndComp
Wire Wire Line
	8450 5300 8450 5250
Wire Wire Line
	8450 5250 8050 5250
Wire Wire Line
	8450 5600 8450 5650
Wire Wire Line
	8450 5650 8050 5650
Connection ~ 8050 5250
Connection ~ 8050 5650
$Comp
L Device:C C55
U 1 1 5EE4ED32
P 8850 5450
F 0 "C55" H 8965 5496 50  0000 L CNN
F 1 "100n" H 8965 5405 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 8888 5300 50  0001 C CNN
F 3 "~" H 8850 5450 50  0001 C CNN
	1    8850 5450
	1    0    0    -1  
$EndComp
Wire Wire Line
	8850 5300 8850 5250
Wire Wire Line
	8850 5250 8450 5250
Wire Wire Line
	8850 5600 8850 5650
Wire Wire Line
	8850 5650 8450 5650
Connection ~ 8450 5250
Connection ~ 8450 5650
$Comp
L power:+3V3 #PWR0174
U 1 1 5EE5D80B
P 7650 5200
F 0 "#PWR0174" H 7650 5050 50  0001 C CNN
F 1 "+3V3" H 7665 5373 50  0000 C CNN
F 2 "" H 7650 5200 50  0001 C CNN
F 3 "" H 7650 5200 50  0001 C CNN
	1    7650 5200
	1    0    0    -1  
$EndComp
Text GLabel 2850 6700 2    50   Input ~ 0
PG_CMB
Text GLabel 2850 7000 2    50   Input ~ 0
PG_CMB
Wire Wire Line
	2700 6700 2850 6700
Wire Wire Line
	2700 7000 2850 7000
Wire Wire Line
	2850 6900 2700 6900
Wire Wire Line
	2700 6800 2850 6800
Wire Wire Line
	2850 7100 2700 7100
Wire Wire Line
	2700 7200 2850 7200
$Comp
L SamacSys_Parts:74HC08DB,118 IC7
U 1 1 5EE9F8AB
P 1700 5250
F 0 "IC7" H 2200 5515 50  0000 C CNN
F 1 "74HC08DB,118" H 2200 5424 50  0000 C CNN
F 2 "SamacSys_Parts:SOP65P775X200-14N" H 3100 5350 50  0001 L CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/74HC_HCT08.pdf" H 3100 5250 50  0001 L CNN
	1    1700 5250
	1    0    0    -1  
$EndComp
Text GLabel 1550 5250 0    50   Input ~ 0
LL_MUTE_LR1
Text GLabel 1550 5350 0    50   Input ~ 0
PG_CMB
Wire Wire Line
	1550 5650 1700 5650
Wire Wire Line
	1550 5250 1700 5250
Wire Wire Line
	1550 5350 1700 5350
Text GLabel 1550 5650 0    50   Input ~ 0
PG_CMB
Text GLabel 1550 5550 0    50   Input ~ 0
LL_MUTE_LR2
Wire Wire Line
	1550 5550 1700 5550
Text GLabel 1550 5450 0    50   Input ~ 0
LL2_MUTE_LR1
Wire Wire Line
	1550 5450 1700 5450
Text GLabel 1550 5750 0    50   Input ~ 0
LL2_MUTE_LR2
Wire Wire Line
	1550 5750 1700 5750
$Comp
L power:GND #PWR0175
U 1 1 5EE9F8C1
P 1600 5950
F 0 "#PWR0175" H 1600 5700 50  0001 C CNN
F 1 "GND" V 1605 5822 50  0000 R CNN
F 2 "" H 1600 5950 50  0001 C CNN
F 3 "" H 1600 5950 50  0001 C CNN
	1    1600 5950
	1    0    0    -1  
$EndComp
Wire Wire Line
	1700 5850 1600 5850
Wire Wire Line
	1600 5850 1600 5950
$Comp
L power:+3V3 #PWR0176
U 1 1 5EE9F8CD
P 2800 5150
F 0 "#PWR0176" H 2800 5000 50  0001 C CNN
F 1 "+3V3" H 2815 5323 50  0000 C CNN
F 2 "" H 2800 5150 50  0001 C CNN
F 3 "" H 2800 5150 50  0001 C CNN
	1    2800 5150
	1    0    0    -1  
$EndComp
Wire Wire Line
	2800 5150 2800 5250
Wire Wire Line
	2800 5250 2700 5250
Text GLabel 2850 5350 2    50   Input ~ 0
PG_CMB
Text GLabel 2850 5650 2    50   Input ~ 0
PG_CMB
Wire Wire Line
	2700 5350 2850 5350
Wire Wire Line
	2700 5650 2850 5650
Text GLabel 2850 5450 2    50   Input ~ 0
LL_MUTE_LR3
Text GLabel 2850 5550 2    50   Input ~ 0
LL2_MUTE_LR3
Wire Wire Line
	2850 5550 2700 5550
Wire Wire Line
	2700 5450 2850 5450
Text GLabel 2850 5750 2    50   Input ~ 0
LL_MUTE_LR4
Text GLabel 2850 5850 2    50   Input ~ 0
LL2_MUTE_LR4
Wire Wire Line
	2850 5750 2700 5750
Wire Wire Line
	2700 5850 2850 5850
$Comp
L SamacSys_Parts:74HC08DB,118 IC14
U 1 1 5EEAEFA5
P 4650 6600
F 0 "IC14" H 5150 6865 50  0000 C CNN
F 1 "74HC08DB,118" H 5150 6774 50  0000 C CNN
F 2 "SamacSys_Parts:SOP65P775X200-14N" H 6050 6700 50  0001 L CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/74HC_HCT08.pdf" H 6050 6600 50  0001 L CNN
	1    4650 6600
	1    0    0    -1  
$EndComp
Text GLabel 4500 6700 0    50   Input ~ 0
PG_CMB
Wire Wire Line
	4500 7000 4650 7000
Wire Wire Line
	4500 6600 4650 6600
Wire Wire Line
	4500 6700 4650 6700
Text GLabel 4500 7000 0    50   Input ~ 0
PG_CMB
Wire Wire Line
	4500 6900 4650 6900
Wire Wire Line
	4500 6800 4650 6800
Wire Wire Line
	4500 7100 4650 7100
$Comp
L power:GND #PWR0177
U 1 1 5EEAEFB9
P 4550 7300
F 0 "#PWR0177" H 4550 7050 50  0001 C CNN
F 1 "GND" V 4555 7172 50  0000 R CNN
F 2 "" H 4550 7300 50  0001 C CNN
F 3 "" H 4550 7300 50  0001 C CNN
	1    4550 7300
	1    0    0    -1  
$EndComp
Wire Wire Line
	4650 7200 4550 7200
Wire Wire Line
	4550 7200 4550 7300
$Comp
L power:+3V3 #PWR0178
U 1 1 5EEAEFC5
P 5750 6500
F 0 "#PWR0178" H 5750 6350 50  0001 C CNN
F 1 "+3V3" H 5765 6673 50  0000 C CNN
F 2 "" H 5750 6500 50  0001 C CNN
F 3 "" H 5750 6500 50  0001 C CNN
	1    5750 6500
	1    0    0    -1  
$EndComp
Wire Wire Line
	5750 6500 5750 6600
Wire Wire Line
	5750 6600 5650 6600
Text GLabel 5800 6700 2    50   Input ~ 0
PG_CMB
Text GLabel 5800 7000 2    50   Input ~ 0
PG_CMB
Wire Wire Line
	5650 6700 5800 6700
Wire Wire Line
	5650 7000 5800 7000
Wire Wire Line
	5800 6900 5650 6900
Wire Wire Line
	5650 6800 5800 6800
Wire Wire Line
	5800 7100 5650 7100
Wire Wire Line
	5650 7200 5800 7200
$Comp
L SamacSys_Parts:74HC08DB,118 IC9
U 1 1 5EEC74A1
P 4650 5250
F 0 "IC9" H 5150 5515 50  0000 C CNN
F 1 "74HC08DB,118" H 5150 5424 50  0000 C CNN
F 2 "SamacSys_Parts:SOP65P775X200-14N" H 6050 5350 50  0001 L CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/74HC_HCT08.pdf" H 6050 5250 50  0001 L CNN
	1    4650 5250
	1    0    0    -1  
$EndComp
Text GLabel 4500 5350 0    50   Input ~ 0
PG_CMB
Wire Wire Line
	4500 5650 4650 5650
Wire Wire Line
	4500 5250 4650 5250
Wire Wire Line
	4500 5350 4650 5350
Text GLabel 4500 5650 0    50   Input ~ 0
PG_CMB
Wire Wire Line
	4500 5550 4650 5550
Wire Wire Line
	4500 5450 4650 5450
Wire Wire Line
	4500 5750 4650 5750
$Comp
L power:GND #PWR0179
U 1 1 5EEC74B3
P 4550 5950
F 0 "#PWR0179" H 4550 5700 50  0001 C CNN
F 1 "GND" V 4555 5822 50  0000 R CNN
F 2 "" H 4550 5950 50  0001 C CNN
F 3 "" H 4550 5950 50  0001 C CNN
	1    4550 5950
	1    0    0    -1  
$EndComp
Wire Wire Line
	4650 5850 4550 5850
Wire Wire Line
	4550 5850 4550 5950
$Comp
L power:+3V3 #PWR0180
U 1 1 5EEC74BF
P 5750 5150
F 0 "#PWR0180" H 5750 5000 50  0001 C CNN
F 1 "+3V3" H 5765 5323 50  0000 C CNN
F 2 "" H 5750 5150 50  0001 C CNN
F 3 "" H 5750 5150 50  0001 C CNN
	1    5750 5150
	1    0    0    -1  
$EndComp
Wire Wire Line
	5750 5150 5750 5250
Wire Wire Line
	5750 5250 5650 5250
Text GLabel 5800 5350 2    50   Input ~ 0
PG_CMB
Text GLabel 5800 5650 2    50   Input ~ 0
PG_CMB
Wire Wire Line
	5650 5350 5800 5350
Wire Wire Line
	5650 5650 5800 5650
Wire Wire Line
	5800 5550 5650 5550
Wire Wire Line
	5650 5450 5800 5450
Wire Wire Line
	5800 5750 5650 5750
Wire Wire Line
	5650 5850 5800 5850
Text GLabel 5800 7200 2    50   Input ~ 0
LL2_MUTE_PA5R
Text GLabel 5800 7100 2    50   Input ~ 0
LL_MUTE_PA5R
Text GLabel 5800 6900 2    50   Input ~ 0
LL2_MUTE_PA5L
Text GLabel 5800 6800 2    50   Input ~ 0
LL_MUTE_PA5L
Text GLabel 4500 7100 0    50   Input ~ 0
LL2_MUTE_PA4R
Text GLabel 4500 6900 0    50   Input ~ 0
LL_MUTE_PA4R
Text GLabel 4500 6800 0    50   Input ~ 0
LL2_MUTE_PA4L
Text GLabel 4500 6600 0    50   Input ~ 0
LL_MUTE_PA4L
Text GLabel 5800 5850 2    50   Input ~ 0
LL2_MUTE_PA3R
Text GLabel 5800 5750 2    50   Input ~ 0
LL_MUTE_PA3R
Text GLabel 5800 5550 2    50   Input ~ 0
LL2_MUTE_PA3L
Text GLabel 5800 5450 2    50   Input ~ 0
LL_MUTE_PA3L
Text GLabel 4500 5750 0    50   Input ~ 0
LL2_MUTE_PA2R
Text GLabel 4500 5550 0    50   Input ~ 0
LL_MUTE_PA2R
Text GLabel 4500 5450 0    50   Input ~ 0
LL2_MUTE_PA2L
Text GLabel 4500 5250 0    50   Input ~ 0
LL_MUTE_PA2L
Text GLabel 2850 7200 2    50   Input ~ 0
LL2_MUTE_PA1R
Text GLabel 2850 7100 2    50   Input ~ 0
LL_MUTE_PA1R
Text GLabel 2850 6900 2    50   Input ~ 0
LL2_MUTE_PA1L
Text GLabel 2850 6800 2    50   Input ~ 0
LL_MUTE_PA1L
NoConn ~ 1700 7100
Wire Wire Line
	1700 6900 1600 6900
Wire Wire Line
	1600 6900 1600 7000
Connection ~ 1600 7200
Wire Wire Line
	1700 7000 1600 7000
Connection ~ 1600 7000
Wire Wire Line
	1600 7000 1600 7200
Wire Wire Line
	4650 4250 4550 4250
Wire Wire Line
	4550 4250 4550 4350
Wire Wire Line
	4650 4350 4550 4350
Connection ~ 4550 4350
Wire Wire Line
	4550 4350 4550 4450
$EndSCHEMATC
