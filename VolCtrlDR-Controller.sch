EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 8
Title "VolCtrlDR-Controller"
Date "2020-01-27"
Rev "0.4"
Comp "T5! DIY Audio"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 2150 1700 1650 750 
U 5DA31ED0
F0 "PowerSupply" 50
F1 "psu.sch" 50
$EndSheet
$Sheet
S 2150 2750 1650 700 
U 5DD8CDD6
F0 "TinkerBoard" 50
F1 "TinkerBoard.sch" 50
$EndSheet
$Sheet
S 4250 1700 1600 750 
U 5DDCEF6A
F0 "PG_combiner" 50
F1 "PG_combiner.sch" 50
$EndSheet
$Sheet
S 4250 2800 1600 650 
U 5DF76B69
F0 "RelayDrivers" 50
F1 "RelayDrivers.sch" 50
$EndSheet
$Sheet
S 6300 1700 1650 750 
U 5DFA6E58
F0 "Connectors" 50
F1 "Connectors.sch" 50
$EndSheet
$Sheet
S 6300 2800 1650 650 
U 5E5989E4
F0 "MCP_GPIOs" 50
F1 "MCP_GPIOs.sch" 50
$EndSheet
$Sheet
S 6300 3850 1650 650 
U 5EBF9173
F0 "I2C_Extenders" 50
F1 "I2C_Extenders.sch" 50
$EndSheet
$EndSCHEMATC
